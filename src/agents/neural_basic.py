import numpy as np

from .base.neural import NeuralAgent, Trainer
from . import import_agent


class BasicNeuralNetworkAgent(NeuralAgent):
    """
    A basic neural agent, working on games with fixed number of states
    """
    name = 'neural-basic'
    configuration = {
        'all': {
            'game': 'nano-static',
            'rounds': 100,
            'learning-rate': 0,
            'epsilon-decay': 0,
        },
        'training': {
            'rounds': 1000,
            'learning-rate': 0.01,
            'epsilon-decay': 0.9999,
        },
        'validation': {},
        'test': {},
    }

    def __init__(self, *args, **kwargs):
        """
        Set the agent's trainer
        """
        super(BasicNeuralNetworkAgent, self).__init__(*args, **kwargs)

        self.trainer = self.get_trainer_class()

    def get_trainer_class(self):
        """
        Get agent's trainer class.

        This method allows dynamic import of agent's libraries
        """
        from keras.models import Sequential
        from keras.layers import Dense
        from keras.optimizers import sgd

        class BasicNeuralNetworkTrainer(Trainer):
            """
            Trainer for the Basic Neural Network agent
            This trainer works on environments with determined number of states
            It pairs states & actions to improve the reward
            """
            def parameterize(self, game, learning_rate, epsilon_decay):
                """
                Calls base method then build a new model if none has been loaded
                """
                super(BasicNeuralNetworkTrainer, self).parameterize(game, learning_rate, epsilon_decay)

                if not self.model:
                    print(f'No model found, creating a new one')
                    model = Sequential()
                    model.add(Dense(24, input_shape=(self.state_size,),
                                    activation='relu'))
                    model.add(Dense(24, activation='relu'))
                    model.add(Dense(self.action_size, activation='linear'))
                    model.compile(loss='mse', optimizer=sgd(lr=self.learning_rate))

                    self.model = model

            def train(self, state, action, reward, next_state, done):
                """
                Store the new reward for this state/action pair then adjust model
                weights with new values
                """
                target = self.model.predict(np.array([state]))[0]

                if done:
                    target[action] = reward
                else:
                    target[action] = reward + self.gamma * np.max(self.model.predict(np.array([next_state])))

                inputs = np.array([state])
                outputs = np.array([target])

                return self.model.fit(inputs, outputs, epochs=1, verbose=0, batch_size=1)

        return BasicNeuralNetworkTrainer()


import_agent(BasicNeuralNetworkAgent)
