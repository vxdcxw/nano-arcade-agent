import os


class KerasModel():
    """
    Base class for agents using Keras library
    """
    def save(self, path):
        """
        Save a keras model to a file
        """
        answer = None

        while answer not in ['y', 'Y', 'n', 'N']:
            answer = input(f'Do you want to save the agent to "{path}" ? [y/N]')

            if answer in ['y', 'Y']:
                print(f'Saving agent model to file {path}')
                self.model.save(path, overwrite=False)

    def load(self, path):
        """
        Load a keras model from a file
        """
        from keras.models import load_model

        if os.path.isfile(path):
            print(f'Loading agent model from file {path}')
            self.model = load_model(path)
