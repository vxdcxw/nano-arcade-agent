import random
import numpy as np

from collections import deque

from .base.neural import NeuralAgent, Trainer
from . import import_agent


class ExperienceReplayNeuralNetworkAgent(NeuralAgent):
    """
    A neural agent based on experience replay.

    It works with games where the number of states is impredictable
    """
    name = 'neural-replay'
    configuration = {
        'all': {
            'game': 'nano-dynamic',
            'rounds': 100,
            'learning-rate': 0,
            'epsilon-decay': 0,
        },
        'training': {
            'rounds': 35000,
            'learning-rate': 0.001,
            'epsilon-decay': 0.999995
        },
        'validation': {},
        'test': {},
    }

    def __init__(self):
        """
        Set the agent's trainer
        """
        super(ExperienceReplayNeuralNetworkAgent, self).__init__()

        self.trainer = self.get_trainer_class()
        self.global_counter = 0
        self.losses = [0]
        self.preparing_rounds = 10000

    def parameterize(self, *args, **kwargs):
        """
        Store trainer's epsilon decay
        """
        super(ExperienceReplayNeuralNetworkAgent, self).parameterize(*args, **kwargs)
        self._epsilon_decay = self.trainer.epsilon_decay
        self.trainer.epsilon_decay = 1

    def get_input(self, state, *args, **kwargs):
        """
        Replay agent needs states per dynamic elements
        """
        state = np.reshape(state, [1, self.trainer.state_size])

        return super(ExperienceReplayNeuralNetworkAgent, self).get_input(state, *args, **kwargs)

    def train(self, state, action, reward, next_state, done=False):
        """
        Reshape and remember environment states
        """
        state = np.reshape(state, [1, self.trainer.state_size])
        next_state = np.reshape(next_state, [1, self.trainer.state_size])

        self.trainer.remember(state, action, reward, next_state, done)

        if self.preparing_rounds < 1:
            self.global_counter += 1

            if self.global_counter % 100 == 0:
                loss = self.trainer.replay()

                if loss:
                    self.losses.append(loss.history['loss'][0])
        else:
            self.preparing_rounds -= 1

            if self.preparing_rounds == 0:
                self.trainer.epsilon_decay = self._epsilon_decay

    def report(self):
        """
        Debug agent's epsilon and last loss
        """
        if self.preparing_rounds > 1:
            print('agent is preparing')
        else:
            print(f'epsilon: {self.trainer.epsilon}, loss: {self.losses[-1]}')

    def get_trainer_class(self):
        """
        Get agent's trainer class.

        This method allows dynamic import of agent's libraries
        """
        from keras.models import Sequential
        from keras.layers import Dense
        from keras.optimizers import Adam

        class ExperienceReplayTrainer(Trainer):
            """
            Trainer for the Experience Replay Neural Network agent
            This agent works on environments with undefined number of states
            Actions, states & rewards are stored in a memory then a replay
            mecanism will compute best results from random samples
            """

            def parameterize(self, game, learning_rate, epsilon_decay, batch_size=30, memory_size=3000):
                """
                Calls base method then build a new model if none has been loaded
                """
                super(ExperienceReplayTrainer, self).parameterize(game, learning_rate, epsilon_decay)

                self.memory = deque(maxlen=memory_size)
                self.batch_size = batch_size

                if not self.model:
                    print(f'No model found, creating a new one')
                    model = Sequential()
                    model.add(Dense(50, input_dim=self.state_size,
                                    activation='relu'))
                    model.add(Dense(30, activation='relu'))
                    model.add(Dense(30, activation='relu'))
                    model.add(Dense(self.action_size, activation='linear'))
                    model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

                    self.model = model

            def replay(self):
                """
                Get best rewards from a sample in the memory then adjust model
                weights with new values
                """
                batch_size = min(self.batch_size, len(self.memory))

                minibatch = random.sample(self.memory, batch_size)

                inputs = np.zeros((batch_size, self.state_size))
                outputs = np.zeros((batch_size, self.action_size))

                for i, (state, action, reward, next_state, done) in enumerate(minibatch):
                    target = self.model.predict(state)[0]

                    if done:
                        target[action] = reward
                    else:
                        target[action] = reward + self.gamma * np.max(self.model.predict([next_state]))

                    inputs[i] = state
                    outputs[i] = target

                return self.model.fit(inputs, outputs, epochs=1, verbose=0, batch_size=batch_size)

            def remember(self, state, action, reward, next_state, done):
                """
                Append environment states/actions/rewards to trainer's memory
                """
                self.memory.append([state, action, reward, next_state, done])

            def predict_actions(self, state):
                """
                Predict values for possible actions
                """
                return self.model.predict(np.array(state))

        return ExperienceReplayTrainer()


import_agent(ExperienceReplayNeuralNetworkAgent)
