class Agent:
    """
    Base class for every agent
    """
    name = None
    trainer = None
    configuration = {
        'all': {
            'game': 'nano-static',
            'rounds': 100,
            'gamma': 0,
            'learning-rate': 0,
            'epsilon-decay': 0,
        },
        'training': {},
        'validation': {},
        'test': {},
    }

    def parameterize(self, game, *args, **kwargs):
        """
        Get agent's available inputs from game, create a dummy trainer
        """
        self.inputs = game.get_inputs()

        if not self.trainer:
            self.trainer = Trainer()

    def finalize(self, *args, **kwargs):
        """
        Abstract
        This method is called after the training
        """
        pass

    def train(self, *args, **kwargs):
        """
        Abstract
        This method is called during train mod
        """
        pass

    def get_input(self, *args, **kwargs):
        """
        Abstract
        Get list of available inputs for agent
        """
        return None

    def exit_callback(self, *args, **kwargs):
        """
        Abstract
        Callback to execute after Ctrl+C
        """
        pass

    def report(self, *args, **kwargs):
        """
        Abstract
        """
        pass


class Trainer:
    """
    A fake Trainer with required attributes
    """
    epsilon = 0

    def parameterize(self, *args, **kwargs):
        pass
