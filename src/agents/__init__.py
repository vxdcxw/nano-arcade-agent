import glob

from os.path import dirname, basename, isfile, join

from .base import Agent

agents = {}


class AgentConfigurationException(Exception):
    pass


def import_agent(agent_class):
    """
    This function check an agent validity and append it to a list of available
    agents.
    """

    if not issubclass(agent_class, Agent):
        raise AgentConfigurationException(f'Agents must derive from base class Agent')

    name = agent_class.name

    if not name:
        raise AgentConfigurationException(f'Agent name is invalid')
    if name in agents:
        raise AgentConfigurationException(f'Name "{name}" is already used')

    agents[name] = agent_class


modules = glob.glob(join(dirname(__file__), "*.py"))
__all__ = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]

from . import *  # noqa
