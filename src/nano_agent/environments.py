import time

from functools import reduce


class Environment:

    def __init__(self, game, agent):
        self.game = game
        self.agent = agent

    def train(self, *args, **kwargs):
        pass


class NanoEnvironment(Environment):
    """
    Environment for 'Nano' game
    """

    def train(self, rounds, report_frequency, verbosity=False):
        """
        Train the agent on the game. The agent is trained for a certain amount
        of rounds.
        """
        print('Training begins !')
        print('-------------------------')
        start = time.process_time()
        rounds_timer = []
        scores = []
        epsilons = []

        for i in range(rounds):
            round_start = time.process_time()
            state = self.game.reset()
            steps = 0
            score = 0
            done = False

            if verbosity == 'DEBUG' and i % report_frequency == 0:
                print(f'\n== Round {i} ==')
                print('-- Start state --')
                self.game.debug_state()

            while not done:
                steps += 1
                action = self.agent.get_input(state=state, step=steps)

                try:
                    next_state = self.game.execute_action(action, propagate=True)
                except (ConnectionResetError, ConnectionRefusedError) as e:
                    print('Connection error, starting a new round')
                    break

                done, reward = self.game.is_finished(steps)
                score += reward
                self.agent.train(state=state, action=action, reward=reward, next_state=next_state, done=done)
                state = next_state

            scores.append(score)
            epsilons.append(self.agent.trainer.epsilon)
            rounds_timer.append(time.process_time() - round_start)

            if i % report_frequency == 0:
                if verbosity == 'DEBUG':
                    print('--- End state ---')
                    self.game.debug_state()
                    print('-----------------')

                self.game.debug()
                self.agent.report()
                duration = rounds_timer[-1]

                print(f'round {i}/{rounds}, {steps} moves, score: {score}, duration: {duration:.6f}s')

        end = time.process_time()
        elapsed = end - start
        average = reduce(lambda a, b: a + b, rounds_timer) / len(rounds_timer)
        min_duration = min(rounds_timer)
        max_duration = max(rounds_timer)

        print('-------------------------')
        print(f'Minimum time: {min_duration:.6f}s')
        print(f'Maximum time: {max_duration:.6f}s')
        print(f'Average time: {average:.6f}s')
        print(f'Training done in {elapsed:.2f}s')
        print('-------------------------')

        return scores, epsilons
