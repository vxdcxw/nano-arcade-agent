# Nano Arcade Agent

This project provides environments for Machine Learning on simple games.

## Requirements
* python >= 3.7
* pip >= 18.0
* pipenv >= 2018.11

## Install

```
pipenv install
```

## Run

```
pipenv run src/run.py
```

## Options

* **-a, --agent**=_AGENT_NAME_: Which agent to run
* **-m, --mode**=_MODE_: which mode to execute. This parameter will mostly change learning parameters
* **-g, --game**=_GAME_: Which game rules to use
* **-r, --rounds**=_COUNT_: Number of rounds to run
* **-v, --verbose**: Debugging verbosity
* **--offline**: Run the game in offline mode (not connected to Unity visualizer)
* **--learning-rate**=_FLOAT_: Agent's learning-rate
* **--gamma**=_FLOAT_: Agent's epsilon decrease (1 is fully random, 0 is no random)
* **--report-frequency**=_COUNT_: Number of rounds between print reports
* **--load**=_PATH_: Load agent at path
* **--save**=_PATH_: Save agent to path
