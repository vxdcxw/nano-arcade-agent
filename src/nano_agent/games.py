import random
import numpy as np

from nano_agent.api import API


def flatten(l):
    return [item for sublist in l for item in sublist]


class Game:
    """
    Base class for any game. This class cannot be used as is and must be
    derived to define specific rules
    """
    def __init__(self, *, is_offline, **kwargs):
        """
        Set online/offline mode and start API
        """
        self.is_offline = is_offline
        self.api = API()

    def get_inputs(self):
        """
        Abstract
        Return the list of available inputs for the game
        """
        return None

    def is_finished(self):
        """
        Abstract
        Return True/False wether the game is finished or not
        """
        return True

    def decode_api_message(self, api_message):
        """
        Virtual
        Decode a message from the API for this game
        """
        return self._compute_state()

    def reset(self):
        """
        Virtual
        Restart the game and returns the new game state
        """
        return self.decode_api_message(self.api.send_message('reset'))

    def execute_action(self, action, propagate=False):
        """
        Virtual
        Execute an action and return API's response
        """
        api_message = self.api.send_message(self.get_inputs()[action], propagate)
        return self.decode_api_message(api_message)


class NanoArcade(Game):
    """
    Base for 'Nano' game. This class cannot be used as is and must be derived
    to define specific rules

    Basicly, this game define a grid, with a player, a goal and holes. The
    player needs to reach the goal with the fewest movements and without
    touching the holes
    """
    name = 'Nano Arcade'

    def __init__(self, options={}, *args, **kwargs):
        """
        Set default values
        """
        super(NanoArcade, self).__init__(*args, **kwargs)

        self.player = ()
        self.goal = ()
        self.holes = ()
        self._states_count = 0
        self.max_steps = options.get('max_steps', 40)
        self.rewards = {
            'victory': options.get('victory_reward', 10),
            'defeat': options.get('defeat_reward', -10),
            'step': options.get('step_reward', -1),
            'out-of-steps': options.get('out-of-movements', -10),
        }

    def get_inputs(self):
        """
        Game inputs:
        - up
        - down
        - left
        - right
        """
        return ['up', 'down', 'left', 'right']

    def get_states_count(self):
        """
        Return total number of different states in this game
        """
        self.reset()
        return self._states_count

    def _get_grid(self, x, y):
        """
        Get grid representation for a specific coordinate
        """
        grid = [
            [0] * self.board_height for i in range(self.board_width)
        ]
        grid[x][y] = 1

        return grid

    def _decode_coordinates(self, el):
        """
        Decode coordinates from an API value
        """
        s = el.split(',')

        return (int(s[0]), int(s[1]))

    def is_finished(self, steps_count):
        """
        Return True/False wether the game is finished and corresponding reward
        """
        is_done = True
        reward = self.rewards['step']

        if self.player == self.goal:
            reward = self.rewards['victory']
        elif self.player in self.holes:
            reward = self.rewards['defeat']
        elif steps_count >= self.max_steps:
            reward = self.rewards['out-of-steps']
        else:
            is_done = False

        return is_done, reward

    def execute_action(self, action, propagate=False):
        """
        Execute agent action and return new game state
        """
        if self.is_offline:
            message = self.get_inputs()[action]

            x, y = self.player

            if message == 'up':
                y += 1
            elif message == 'down':
                y -= 1
            elif message == 'right':
                x += 1
            elif message == 'left':
                x -= 1

            if x >= 0 and x < self.board_width and \
               y >= 0 and y < self.board_height:
                self.player = (x, y)

            return self._compute_state()

        else:
            return super(NanoArcade, self).execute_action(action, propagate)

    def debug(self):
        """
        Output to console wether the player won or lost
        """
        fmt = (2, 'won') if self.player == self.goal else (1, 'lost')
        print('\033[9{}m-- You {} !\033[0m'.format(*fmt))

    def debug_state(self):
        """
        Output to console game state
        """
        print(f'player: {self.player}')
        print(f'goal: {self.goal}')
        print(f'holes: {self.holes}')
        print(f'board: ({self.board_width}, {self.board_height})')


class NanoArcadeStatic(NanoArcade):
    """
    Static variant of 'Nano' Game.

    The grid size is fixed, the player, goal and holes always appear at the same
    position
    """
    configuration = {
        'board': {
            'width': 9,
            'height': 9,
        },
        'player': (4, 4),
        'goal': (8, 8),
        'holes': [(0, 4)],
    }

    def __init__(self, options={}, *args, **kwargs):
        """
        Set board width/height from configuration
        """
        super(NanoArcadeStatic, self).__init__(*args, **kwargs)

        self.board_width = self.configuration['board']['width']
        self.board_height = self.configuration['board']['height']
        self._states_count = self.board_width * self.board_height

    def _compute_state(self):
        """
        Returns a flat grid of the board with player position
        """
        return flatten(self._get_grid(*self.player))

    def decode_api_message(self, message):
        """
        Get new player/goal/holes positions, if available get board's size.
        Returns the new game state
        """
        try:
            self.player = self._decode_coordinates(message['player'])
            self.goal = self._decode_coordinates(message['goal'][0])
            self.holes = [self._decode_coordinates(x) for x in message['hole']]

            board = message.get('board')

            if board:
                w, h = self._decode_coordinates(board[0])
                self.board_width = w + 1
                self.board_height = h + 1
                self._states_count = self.board_width * self.board_height

        except KeyError as e:
            print(f'Error while decoding api message: {e}')

        return self._compute_state()

    def reset(self):
        """
        Restart the game and returns the new game state
        """
        if self.is_offline:
            self.player = self.configuration['player']
            self.goal = self.configuration['goal']
            self.holes = self.configuration['holes'][0]

            return self._compute_state()
        else:
            return super(NanoArcade, self).reset()


class NanoArcadeDynamic(NanoArcade):
    """
    Dynamic variant of 'Nano' Game.

    The grid size, the player, goal and holes positions are randomized after
    each success
    """
    configuration = {
        'board': {
            'min-width': 4,
            'max-width': 8,
            'min-height': 4,
            'max-height': 8,
        },
        'holes_count': 2,
    }

    def __init__(self, options={}, *args, **kwargs):
        """
        Set board width/height and holes count from configuration
        """
        super(NanoArcadeDynamic, self).__init__(*args, **kwargs)

        board = self.configuration['board']
        self.board_width = random.randrange(board['min-width'], board['max-width'] + 1)
        self.board_height = random.randrange(board['min-height'], board['max-height'] + 1)
        self.holes_count = random.randrange(1, 3)
        self._states_count = self.board_width * self.board_height * (2 + self.holes_count)

    def _compute_state(self):
        """
        Returns grids for the player/goal/holes positions
        """
        return flatten(np.reshape(
                [self._get_grid(x, y) for (x, y) in
                    [self.player, self.goal, *[hole for hole in self.holes]]],
                (1, self._states_count)
                ))

    def decode_api_message(self, message):
        """
        Get new player/goal/holes positions, if available get board's size.
        Returns the new game state
        """
        try:
            self.player = self._decode_coordinates(message['player'])
            self.goal = self._decode_coordinates(message['goal'][0])
            self.holes = [self._decode_coordinates(x) for x in message['hole']]

            dynamic_parameters = 2 + len(self.holes)

            board = message.get('board')

            if board:
                w, h = self._decode_coordinates(board[0])
                self.board_width = w + 1
                self.board_height = h + 1
                self._states_count = self.board_width * self.board_height * dynamic_parameters

        except KeyError as e:
            print(f'Error while decoding api message: {e}')

        return self._compute_state()

    def reset(self):
        """
        Restart the game and returns the new game state
        """
        if self.is_offline:
            size = self.board_width * self.board_height
            pool = [(x, y) for x in range(self.board_width)
                    for y in range(self.board_height)]
            self.player = pool.pop(random.randrange(size))
            self.goal = pool.pop(random.randrange(size - 1))
            self.holes = [pool.pop(random.randrange(len(pool))) for x in range(self.holes_count)]

            return self._compute_state()
        else:
            return super(NanoArcade, self).reset()
