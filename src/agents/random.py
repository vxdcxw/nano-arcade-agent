import random

from . import import_agent
from .base import Agent


class RandomAgent(Agent):
    """
    This agent does nothing special, it only returns a random input from
    available inputs
    """
    name = 'random'

    def get_input(self, *args, **kwargs):
        return random.randrange(len(self.inputs))


import_agent(RandomAgent)
