#!/usr/bin/env python

import argparse
import signal

from utils import agents, modes, games, environments, \
                   visualize_scores, get_agent_settings, signal_handler, set_exit_callback


if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal_handler)
    print('== Press Ctrl+C to exit ==')

    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action="store_true", help='Verbosity')
    parser.add_argument('-m', '--mode', choices=modes, default=modes[0])
    parser.add_argument('-a', '--agent', choices=agents.keys(), default=list(agents.keys())[0])
    parser.add_argument('-g', '--game', choices=games.keys())
    parser.add_argument('-o', '--offline', action="store_true", help='Run offline training')
    parser.add_argument('--learning-rate', type=float, help='Learning rate')
    parser.add_argument('--gamma', type=float,
                        help='Actualisation factor (importance of future rewards)')
    parser.add_argument('-r', '--rounds', type=int, help='Number of rounds to train the agent')
    parser.add_argument('--report-frequency', type=int, default=200,
                        help='Number of rounds between reports')
    parser.add_argument('--load', help='Path to export trained agent')
    parser.add_argument('--save', help='Path to import trained agent')
    args = parser.parse_args()

    game_name, rounds, gamma, epsilon_decay, lr = get_agent_settings(args)

    verbosity = 'DEBUG' if args.verbose else 'INFO'

    print('== Training parameters ==')
    print(f'Agent: {args.agent}')
    print(f'Game: {game_name}')
    print(f'Rounds: {rounds}')
    print(f'Gamma: {gamma}')
    print(f'Epsilon_decay: {epsilon_decay}')
    print(f'Learning rate: {lr}')
    print('=========================')

    agent = agents[args.agent]()
    game = games[game_name](is_offline=args.offline)
    environment = environments[game_name](game, agent)

    agent.parameterize(game,
                       gamma=gamma,
                       learning_rate=lr,
                       epsilon_decay=epsilon_decay,
                       import_path=args.load,
                       export_path=args.save,
                       )

    set_exit_callback(agent.exit_callback)

    scores = environment.train(rounds, args.report_frequency, verbosity)

    agent.finalize()

    visualize_scores(scores)
