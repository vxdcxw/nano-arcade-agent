import numpy as np
import random

from . import Agent
from .keras import KerasModel


class NeuralAgent(Agent):
    """
    Base class for agents using neural networks
    """
    def __init__(self):
        """
        Set a default trainer
        """
        super(NeuralAgent, self).__init__()

        self.trainer = Trainer()

    def parameterize(self, game, learning_rate, epsilon_decay, import_path=None, export_path=None, *args, **kwargs):
        """
        Load a model if import_path is defined
        """
        super(NeuralAgent, self).parameterize(game)

        if import_path:
            self.trainer.load(import_path)

        self.export_path = export_path
        self.trainer.parameterize(game, learning_rate, epsilon_decay)

    def finalize(self):
        """
        Action to execute after training
        """
        if self.export_path:
            self.trainer.save(self.export_path)

    def exit_callback(self):
        """
        Action to execute when Ctrl+C is pressed
        """
        self.finalize()

    def get_input(self, state, *args, **kwargs):
        """
        Returns the best action for this state from the trainer
        """
        return self.trainer.get_best_action(state, *args, **kwargs)

    def train(self, state, action, reward, next_state, done=False):
        """
        Train the agent using its trainer
        """
        return self.trainer.train(state, action, reward, next_state, done)

    def report(self):
        """
        Debug agent's epsilon
        """
        print(f'epsilon: {self.trainer.epsilon}')


class Trainer(KerasModel):
    """
    Base Trainer for NeuralAgents
    """
    model = None

    def parameterize(self, game, learning_rate, epsilon_decay):
        self.state_size = game.get_states_count()
        self.action_size = 4
        self.gamma = 0.9
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.epsilon_decay = epsilon_decay
        self.learning_rate = learning_rate

    def get_best_action(self, state, rand=True, *args, **kwargs):
        """
        Returns the best action found for this state with a decreasing
        probability to get a random action
        """
        self.epsilon *= self.epsilon_decay

        if rand and np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)

        act_values = self.predict_actions(state)
        action = np.argmax(act_values[0])

        return action

    def predict_actions(self, state):
        """
        Predict values for possible actions of the model
        """
        return self.model.predict(np.array([state]))
