import matplotlib.pyplot as plt
import numpy as np
import sys

from nano_agent.environments import NanoEnvironment
from nano_agent.games import NanoArcadeStatic, NanoArcadeDynamic
from agents import agents


exit_callback = None


environments = {
    'nano-static': NanoEnvironment,
    'nano-dynamic': NanoEnvironment,
}

games = {
    'nano-static': NanoArcadeStatic,
    'nano-dynamic': NanoArcadeDynamic,
}

modes = [
    'training',
    'validation',
    'test'
]


def smooth(vector, width=10):
    """
    Smooth up values for nicer visualisations
    """
    return np.convolve(vector, [1/width] * width, mode='valid')


def visualize_scores(scores):
    """
    Display a graph of agent's results
    """
    print('Printing results.')

    scores, epsilons = np.array(scores)

    score = smooth(scores, width=500)
    epsilon = smooth(epsilons, width=500)

    fig, ax1 = plt.subplots()
    ax1.plot(score)
    ax1.set_xlabel('Round')
    ax1.set_ylabel('Score')
    ax2 = ax1.twinx()
    ax2.plot(epsilon, color='r')
    ax2.set_ylabel('Epsilon', color='r')
    ax2.tick_params('y', colors='r')
    plt.title('Score and epsilon over training')

    plt.show()


def set_exit_callback(callback):
    """
    Set a function to call as callback when catching Ctrl+C signal
    """
    global exit_callback

    exit_callback = callback


def signal_handler(sig, frame):
    """
    Handling Ctrl+C
    """
    print('\nStopping training')

    if exit_callback:
        exit_callback()

    sys.exit(0)


def get_setting(arg, key, conf, defaults):
    """
    One-liner to get value from conf, defaults if value is not found
    """
    return arg if arg else conf.get(key, defaults.get(key))


def get_agent_settings(args):
    """
    Build a basic configuration from CLI arguments
    """
    conf = agents[args.agent].configuration
    settings = conf[args.mode]
    defaults = conf['all']
    game = get_setting(args.game, 'game', settings, defaults) or list(games.keys())[0]
    rounds = get_setting(args.rounds, 'rounds', settings, defaults) or 100
    gamma = get_setting(args.gamma, 'gamma', settings, defaults) or 0
    y = get_setting(args.gamma, 'epsilon-decay', settings, defaults) or 0
    lr = get_setting(args.learning_rate, 'learning-rate', settings, defaults) or 0
    return game, rounds, gamma, y, lr
