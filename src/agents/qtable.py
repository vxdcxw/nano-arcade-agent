import numpy as np

from . import import_agent
from .base import Agent


class QTableAgent(Agent):
    """
    A simple Q-Table agent
    """
    name = 'qtable'
    configuration = {
        'all': {
            'game': 'nano-static',
            'rounds': 100,
            'gamma': 0,
            'learning-rate': 0,
        },
        'training': {
            'gamma': 0.99,
            'learning-rate': 0.85,
        },
        'validation': {},
        'test': {}
    }

    def parameterize(self, game, learning_rate, gamma, *args, **kwargs):
        """
        Set training parameters
        """
        super(QTableAgent, self).parameterize(game)

        self.states_n = game.get_states_count()
        self.actions_n = len(self.inputs)
        self.lr = learning_rate
        self.y = gamma
        self.reward_sum = 0
        self.reward_sum_list = []
        self.Q = np.zeros([self.states_n, self.actions_n])

    def get_input(self, state, step, *args, **kwargs):
        """
        Returns the best input found with a decreasing probability to get a
        random input
        """
        self.s = state.index(1)

        s = self.s
        self.Q2 = self.Q[s, :] + np.random.randn(1, self.actions_n) * (1. / (step + 1))
        self.a = np.argmax(self.Q2)

        return self.a

    def train(self, state, next_state, reward, *args, **kwargs):
        """
        Train the agent. For this state/action compute the reward function
        Store the reward in a list to compute score average later
        Store the new state in 's'
        """
        self.s1 = next_state.index(1)
        a = self.a
        lr = self.lr
        y = self.y
        s = self.s
        s1 = self.s1
        self.Q[s, a] = self.Q[s, a] + lr * (reward + y * np.max(self.Q[s1, :]) - self.Q[s, a])
        self.reward_sum += reward
        self.s = s1

    def report(self):
        """
        Debug average reward over time
        """
        self.reward_sum_list.append(self.reward_sum)
        self.reward_sum = 0

        print('Score over time: ' + str(sum(self.reward_sum_list[-100:]) / 100.0))


import_agent(QTableAgent)
