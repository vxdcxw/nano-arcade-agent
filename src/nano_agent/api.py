import os
import socket
import json
import time


class API:

    def __init__(self):
        """
        Setup API from environment variables
        """
        self.ip = os.environ.get('HOST_IP', 'localhost')
        self.port = int(os.environ.get('HOST_PORT', 11008))
        self.buff_size = int(os.environ.get('BUFF_SIZE', 1024))

    def wait_for_message(self):
        """
        Connect to API socket, wait for connection and return received datas as
        json
        """
        data = ''

        while not data:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((self.ip, self.port))
                data = self.socket.recv(self.buff_size)
                self.socket.close()
            except (ConnectionResetError, ConnectionRefusedError) as e:
                self.socket.close()
                raise e

        return json.loads(data.decode('utf-8'))

    def send_message(self, message, propagate=False):
        """
        Connect to API socket, send message, and get API response (json)
        """
        api_response = None

        while not api_response:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((self.ip, self.port))
                self.socket.sendall(bytearray(message, 'utf8'))
                self.socket.close()

                api_response = self.wait_for_message()
            except (ConnectionResetError, ConnectionRefusedError) as e:
                self.socket.close()

                if propagate:
                    raise e
                else:
                    print(f'API is not reachable: {e}')

                time.sleep(2)

        return api_response
